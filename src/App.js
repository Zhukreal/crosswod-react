import React, {Component} from "react";
import "./libs/grid/grid.css";
import "./App.css"
import Home from "./components/Home";
import NotFound from "./components/NotFound";
import Club from "./components/Club";
import Schedule from "./components/Schedule";
import Package from "./components/Package";
import PackageOne from "./components/PackageOne";
import Training from "./components/Training";
import Board from "./components/Board";
import {Route, Switch} from "react-router-dom";
import './fonts/montserrat/montserrat.css'
import {getClub} from "./actions/api";

class App extends Component {

    render() {
        return (
            <div className="App">
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route exact path='/404' component={NotFound}/>
                    <Route exact path='/clubs/:id' component={Club}/>
                    <Route path='/clubs/:id/schedule' component={Schedule}/>
                    <Route exact path='/clubs/:id/package' component={Package}/>
                    <Route path='/clubs/:id/package/:id2' component={PackageOne}/>

                    <Route exact path='/training/:id' component={Training}/>
                    <Route path='/training/:id/board' component={Board}/>

                    {/*<Route exact path='/clubs/:id' render={(props) => (*/}
                        {/*<Club type={this.state.type}*/}
                              {/*platform={this.state.platform}*/}
                        {/*/>)}*/}
                    {/*/>*/}

                    {/*<Route path='/training/:id' render={(props) => (*/}
                        {/*<Training type={this.state.type}*/}
                              {/*platform={this.state.platform}*/}
                        {/*/>)}*/}
                    {/*/>*/}

                    {/*<Route path='/training/:id/board' render={(props) => (*/}
                        {/*<Board type={this.state.type}*/}
                              {/*platform={this.state.platform}*/}
                        {/*/>)}*/}
                    {/*/>*/}

                </Switch>
            </div>
        );
    }
}

export default App;



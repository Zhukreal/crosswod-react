import axios from 'axios'

export const getClub = (id) => {
    return axios.get('/v1/clubs/' + id)
}
export const getProgram = (id, id2) => {
    return axios.get('/v1/share/package/' + id)
}
export const getTraining = (id) => {
    return axios.get('/v1/share/training/' + id)
}


import React from 'react'
import {getTraining} from '../actions/api'
import moment from 'moment'
import 'moment/locale/ru'
import packageJSON from '../../package.json'
import Loader  from 'react-loader'
import device from "../actions/getDevice";
import Header from './Header'
import Footer from './Footer'



export default class Training extends React.Component {
    constructor (props){
        super()
        this.state = {
            history: props.history,
            props: props,
            data: {},
            loaded: false
        }

    }

    componentDidMount(){
        getTraining(this.state.props.match.params.id).then((res)=>{
            console.log('res', res);
            this.setState({data: res.data, loaded: true})
        })
            .catch(error => {
                console.error('error', error);
                this.setState({loaded: true})
            })
    }



    render() {
        let data = this.state.data;
        // moment.locale('ru');
        return  (
            <div className='c-container'>

                <Header></Header>


                <div className="c-card">
                    <div className="block-info-club">
                        <img src={packageJSON.proxy + '/' + data.clubLogo} alt=""/>
                    </div>
                    <div className="club-title">{data.clubTitle}</div>
                    <div className="club-description">{data.clubDescription}</div>



                    <div className="block-info-club">
                        <div className="divider"></div>
                    </div>
                    <div className="btn-training">Тренировка</div>
                    <div className="c-train-title">{data.trainingTitle}</div>
                    <div>{moment(data.executeDate).format('D MMMM YYYY')}</div>


                    {device.platform && device.platform == 'iOS' &&
                    <a className="w-btn btn-crosswod fix" href={"crosswod://training/" + data.trainingId}>Открыть в приложении</a>
                    }
                    {device.platform && device.platform == 'Android' &&
                    <a className="w-btn btn-crosswod fix" href={"crosswod://deeplink/training/" + data.trainingId}>Открыть в приложении</a>
                    }

                </div>



                <Footer></Footer>


                <Loader loaded={this.state.loaded}></Loader>

            </div>
        )
    }
}

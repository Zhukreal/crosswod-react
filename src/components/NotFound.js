import React from 'react'
import logo from './../img/logo.png'
import appStore from './../img/appStore.svg'
import googlePlay from './../img/googlePlay2.png'
import device from '../actions/getDevice'



export default class NotFound extends React.Component {
    constructor (props){
        super()
        this.state = {
            history: props.history,
            props: props
        }

    }

    componentDidMount(){

    }

    render() {
        return  (
            <div className='c-container'>
                <div className="c-header clearfix">
                    <div className="ch-left">
                        <img src={logo} alt=""/>
                    </div>
                    <div className="ch-right">
                        <div className="chr-1">CrossWOD</div>
                        <div className="chr-2">Кроссфит на ладони</div>
                        <div className="chr-3"><a href="https://crosswod.ru/">crosswod.ru</a></div>
                    </div>
                </div>

                <div className='block-for-not'>
                    <div className="block-404">404</div>
                    <div className="block-not-found">Такой страницы не существует</div>
                    <div className="block-link"><a href="https://crosswod.ru/">crosswod.ru</a></div>
                </div>



                {/*{device.platform &&*/}
                {/*<div className="c-footer">*/}
                    {/*/!*<div className="c-or">или</div>*!/*/}

                    {/*{device.platform == 'iOS' &&*/}
                    {/*<a className="w-btn btn-app-store"*/}
                       {/*href="https://itunes.apple.com/by/app/crosswod/id1187928230?mt=8">*/}
                        {/*<img src={appStore} alt=""/>*/}
                    {/*</a>*/}
                    {/*}*/}
                    {/*{device.platform == 'Android' &&*/}
                    {/*<a className="w-btn btn-google-play"*/}
                       {/*href="https://play.google.com/store/apps/details?id=com.crosswod&hl=ru">*/}
                        {/*<img src={googlePlay} alt=""/>*/}
                    {/*</a>*/}
                    {/*}*/}
                {/*</div>*/}
                {/*}*/}


                {/*{!device.platform &&*/}
                {/*<div className="c-footer">*/}
                    {/*/!*ссылки для десктопа*!/*/}
                    {/*/!*<div className="c-or">или</div>*!/*/}


                    {/*<a className="w-btn btn-app-store"*/}
                       {/*href="https://itunes.apple.com/by/app/crosswod/id1187928230?mt=8">*/}
                        {/*<img src={appStore} alt=""/>*/}
                    {/*</a>*/}
                    {/*<a className="w-btn btn-google-play"*/}
                       {/*href="https://play.google.com/store/apps/details?id=com.crosswod&hl=ru">*/}
                        {/*<img src={googlePlay} alt=""/>*/}
                    {/*</a>*/}

                {/*</div>*/}
                {/*}*/}


            </div>
        )
    }
}

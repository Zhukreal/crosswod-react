import React from 'react'
import appStore from './../img/appStore.svg'
import googlePlay from './../img/googlePlay2.png'
import device from '../actions/getDevice'


export default class Footer extends React.Component {
    constructor (props){
        super()
        this.state = {
            history: props.history,
            props: props,
            club: {},
            loaded: false
        }

    }

    componentDidMount(){

    }


    render() {
        return  (
            <div>
                {device.platform &&
                <div className="c-footer">
                    <div className="c-or">или</div>

                    {device.platform == 'iOS' &&
                    <a className="w-btn btn-app-store"
                       href="https://itunes.apple.com/by/app/crosswod/id1187928230?mt=8">
                        <img src={appStore} alt=""/>
                    </a>
                    }
                    {device.platform == 'Android' &&
                    <a className="w-btn btn-google-play"
                       href="https://play.google.com/store/apps/details?id=com.crosswod&hl=ru">
                        <img src={googlePlay} alt=""/>
                    </a>
                    }
                </div>
                }


                {!device.platform &&
                <div className="c-footer desctop">
                    {/*ссылки для десктопа*/}


                    <a className="w-btn btn-app-store"
                       href="https://itunes.apple.com/by/app/crosswod/id1187928230?mt=8">
                        <img src={appStore} alt=""/>
                    </a>
                    <a className="w-btn btn-google-play"
                       href="https://play.google.com/store/apps/details?id=com.crosswod&hl=ru">
                        <img src={googlePlay} alt=""/>
                    </a>

                </div>
                }
            </div>
        )
    }
}

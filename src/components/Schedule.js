import React from 'react'
import {getClub} from '../actions/api'
import packageJSON from '../../package.json'
import Loader  from 'react-loader'
import MetaTags from 'react-meta-tags';
import device from '../actions/getDevice'
import Header from './Header'
import Footer from './Footer'


export default class Schedule extends React.Component {
    constructor (props){
        super()
        this.state = {
            history: props.history,
            props: props,
            club: {},
            loaded: false
        }

    }

    componentDidMount(){
        getClub(this.state.props.match.params.id).then((res)=>{
            this.setState({club: res.data, loaded: true})
        })
            .catch(error => {
                console.error('error', error);
                this.setState({loaded: true})
            })

        console.log('device', device)

    }


    render() {
        return  (
            <div className='c-container'>
                {this.state.club.id &&
                <MetaTags>
                    <title>{this.state.club.title}</title>
                    <meta property="og:title" content={this.state.club.title}/>
                    <meta property="og:type" content="website"/>
                    {/*<meta property="fb:app_id" content="1838633282837270"/>*/}
                    <meta property="og:url" content={window.location.href}/>
                    <meta name="description" content={this.state.club.description}/>
                    <meta property="og:image" content={packageJSON.proxy + '/' + this.state.club.logoUrl}/>
                </MetaTags>
                }

                <Header></Header>



                {this.state.club.id &&
                <div className="c-card">
                    <div className="block-info-club">
                        <img src={packageJSON.proxy + '/' + this.state.club.logoUrl} alt=""/>
                    </div>
                    <div className="club-title">{this.state.club.title}</div>
                    <div className="club-description">{this.state.club.description}</div>
                    <div className="block-info-club">
                        <div className="divider"></div>
                    </div>
                    <div className="btn-training">Расписание клуба</div>


                    {device.platform && device.platform == 'iOS' &&
                    <a className="w-btn btn-crosswod" href={"crosswod://clubs/" + this.state.club.clubId + '/schedule'}>Открыть в приложении</a>
                    }
                    {device.platform && device.platform == 'Android' &&
                    <a className="w-btn btn-crosswod" href={"crosswod://deeplink/clubs/" + this.state.club.clubId + '/schedule'}>Открыть в приложении</a>
                    }

                </div>
                }


                <Footer></Footer>


                <Loader loaded={this.state.loaded}></Loader>

            </div>
        )
    }
}
